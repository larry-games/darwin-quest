# Hitbox Game Jam : Tours 2018

This project has been developped during the [Hitbox Game Jam 2018](https://hitboxmakers.fr/).

The main theme was : "The Evolution".

We've decided to develop a 2D Space Shooter Game based on the Charles Darwin universe.

You can retrieve the current code and project directly from itch.io website :
[Darwin's Quest](https://squalex.itch.io/darwins-quest).

## Commands

| Command | Action |
| ------- | ------ |
| Space   | Shoot  |
| Arrow Keys   | Move  |

## Launch the game

Download https://processing.org/download/ and start shoothemup.pde

## Game images

![Splash Screen](images/splashscreen.png)

![Game Screen](images/gamescreen.png)